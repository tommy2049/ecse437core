/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Renaming</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.CORERenaming#getRenames <em>Renames</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.core.CorePackage#getCORERenaming()
 * @model
 * @generated
 */
public interface CORERenaming<T extends COREModelElement> extends COREModelElementComposition<T>, CORENamedElement {
    /**
     * Returns the value of the '<em><b>Renames</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Renames</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Renames</em>' reference.
     * @see #setRenames(COREModelElement)
     * @see ca.mcgill.sel.core.CorePackage#getCORERenaming_Renames()
     * @model required="true"
     * @generated
     */
    COREModelElement getRenames();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.core.CORERenaming#getRenames <em>Renames</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Renames</em>' reference.
     * @see #getRenames()
     * @generated
     */
    void setRenames(COREModelElement value);

} // CORERenaming
