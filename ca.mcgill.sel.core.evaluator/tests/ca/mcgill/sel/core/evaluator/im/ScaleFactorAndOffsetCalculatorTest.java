package ca.mcgill.sel.core.evaluator.im;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.CorePackage;

/**
 * Test class that runs the scale factor and offset calculation algorithm on predefined test cases.
 * JUnit parameterized tests allows running the same test case with different inputs from the predefined set
 * of input variables.
 * Test case names are determined by checking the .core files in the models folder and tests are ran for 
 * each .core model. 
 * Expected results are defined in the corresponding .csv file for each .core file with the following format:
 * ModelName,ScaleFactorAsFloat,OffsetAsFloat
 * @author mduran35
 */
@RunWith(Parameterized.class)
public class ScaleFactorAndOffsetCalculatorTest {

    private static final String TEST_MODEL_PATH = "tests/models/im/";
    //Error margin is set to 0.5 to match the EVALUATION_TOLERANCE_VALUE in ForwardPropagationAlgorithm class.
    private static final double ERROR_MARGIN = 0.1;
    //False scale factor and offset values are set to each impact model element before running the algorithm.
    private static final float FALSE_SCALE_FACTOR = -4.07f;
    private static final float FALSE_OFFSET = -27.05f;
    //expectedValueTable contains the expected results extracted from csv files.
    private static ExpectedEvaluationResults expectedValueTable;
    
    /**
     * Test case name used as a parameter for each individual test case.
     * Needs to be public for JUnit to set the value for each case (see https://github.com/junit-team/junit/pull/737).
     */
    @Parameter
    public String testCaseName;
    
    private COREConcern concern;
    
    /**
     * Common initialization done before starting the tests.
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        // Initialize ResourceManager.
        ResourceManager.initialize();
        
        // Initialize packages.
        CorePackage.eINSTANCE.eClass();
    }
    
    /**
     * Common finalization operations done after each test case.
     */
    @After
    public void tearDown() {
        ResourceManager.unloadResource(concern.eResource());
    }
    
    /**
     * Common finalization operation after all test cases are done.
     */
    @AfterClass
    public static void tearDownAfterClass() {
        expectedValueTable = null;
    }
    
    /**
     * Function that sets the parameters for test cases.
     * @return the collection of inputs.
     */
    @Parameters(name = "Running on model: {0}.core")
    public static Collection<Object[]> testCaseNames() {
        expectedValueTable = new ExpectedEvaluationResults();
        assertTrue(expectedValueTable.initialize(TEST_MODEL_PATH));
        
        Object[] names = expectedValueTable.getTestCaseNames().toArray();
        Object[][] list = new Object[names.length][1];
        int i = 0;
        for (Object anObject : names) {
            list[i][0] = anObject;
            i++;
        }
        return Arrays.asList(list);
    }
    
    /**
     * Test case that runs the evaluation on a goal model with single goal and all positive values.
     * This test will run for as many times as the number of parameters defined (.core files).
     */
    @Test
    public void testScaleFactorAndOffsetEvaluation() {
        //Load the test concern for this case.
        String testName = testCaseName;
        
        concern = (COREConcern) ResourceManager.loadModel(TEST_MODEL_PATH + testName + ".core");
        COREImpactModel impactModel = concern.getImpactModel();
        COREFeatureModel featureModel = concern.getFeatureModel();
        
        //Set the existing scale factor and offsets to false values before running the algorithm.
        for (COREImpactNode aNode : impactModel.getImpactModelElements()) {
            aNode.setScalingFactor(FALSE_SCALE_FACTOR);
            aNode.setOffset(FALSE_OFFSET);
        }
        
        //Do the evaluation.
        ScaleFactorAndOffsetCalculator.calculateScaleFactorsAndOffsets(impactModel, featureModel);
        
        //Check the impact model elements for correctness.
        for (COREImpactNode aNode : impactModel.getImpactModelElements()) {
            assertEquals(expectedValueTable.getScaleFactorForElementInTest(aNode.getName(), testName),
                    aNode.getScalingFactor(), ERROR_MARGIN);
            assertEquals(expectedValueTable.getOffsetForElementInTest(aNode.getName(), testName),
                    aNode.getOffset(), ERROR_MARGIN);
        }
    }
}
