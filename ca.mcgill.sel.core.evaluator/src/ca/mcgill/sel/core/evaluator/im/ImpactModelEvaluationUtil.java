package ca.mcgill.sel.core.evaluator.im;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ca.mcgill.sel.core.COREContribution;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureImpactNode;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.COREImpactNode;

/**
 * Class that contains utility functions for the impact model evaluation.
 * @author mduran35
 *
 */
public final class ImpactModelEvaluationUtil {

    private static final float MAX_POSSIBLE_RELATIVE_VALUE = 100.0f;
    private static final float MIN_POSSIBLE_RELATIVE_VALUE = 0.0f;
    
    /**
     * Constructor.
     */
    private ImpactModelEvaluationUtil() {
        
    }
    
    /**
     * Getter.
     * @return the maxPossibleRelativeValue
     */
    static float getMaxPossibleRelativeValue() {
        return MAX_POSSIBLE_RELATIVE_VALUE;
    }

    /**
     * Getter.
     * @return the minPossibleRelativeValue
     */
    static float getMinPossibleRelativeValue() {
        return MIN_POSSIBLE_RELATIVE_VALUE;
    }
    
    /**
     * Calculating the contribution of a child goal to a parent. 
     * @param child 
     * @param parent 
     * @return the contribution value.
     */
    static float getWeightedContributionBetweenElements(COREImpactNode child, COREImpactNode parent) {
        float weightedContribution = 0.0f;
        for (COREContribution contribution : child.getOutgoing()) {
            if (contribution.getImpacts().equals(parent)) {
                weightedContribution = contribution.getRelativeWeight() / ForwardPropagationAlgorithm.PROPAGATION_SCALE;
                break;
            }
        }
        return weightedContribution;
    }
    
    /**
     * Function to calculate scale factor and offset values given min and max achievable values.
     * @param element 
     * @param maxAchievable 
     * @param minAchievable 
     */
    static void calculateAndSetScaleFactorAndOffsetValues(COREImpactNode element,
            float maxAchievable, float minAchievable) {
        float scaleFactor = (float) 0.0;
        float offset = (float) 0.0;
        
        if (maxAchievable == minAchievable) {
            scaleFactor = (float) 1.0;
            offset = MAX_POSSIBLE_RELATIVE_VALUE - minAchievable;
        } else {
            scaleFactor = (MAX_POSSIBLE_RELATIVE_VALUE - MIN_POSSIBLE_RELATIVE_VALUE) / (maxAchievable - minAchievable);
            offset = ((MIN_POSSIBLE_RELATIVE_VALUE * maxAchievable) - (MAX_POSSIBLE_RELATIVE_VALUE * minAchievable))
                    / (maxAchievable - minAchievable);
        }
        element.setScalingFactor(scaleFactor);
        element.setOffset(offset);
    }
    
    /**
     * Function to adjust the satisfaction value.
     * @param satisfaction 
     * @param scaleFactor 
     * @param offset 
     * @return the adjusted satisfaction value.
     */
    static float scaleAndOffsetSatisfaction(float satisfaction, float scaleFactor, float offset) {
        return (satisfaction * scaleFactor) + offset;
    }
    
    /**
     * Checks if the indexed evaluation already exists in the table.
     * @param indexToAsk is the planned index.
     * @param calculatedIndexes is the existing table.
     * @return true if index exists.
     */
    static boolean checkExistingCalculatedIndexes(int[] indexToAsk, 
            Set<String> calculatedIndexes) {
        String indexToAskText = "";
        for (int a : indexToAsk) {
            indexToAskText += a + ",";
        }
        if (indexToAskText.length() > 1) {
            indexToAskText = indexToAskText.substring(0, indexToAskText.lastIndexOf(","));
        } 
        return calculatedIndexes.contains(indexToAskText);
    }
    
    /**
     * Function to calculate the next best indexes to be asked to the contributor children.
     * @param goal 
     * @param evaluationInfo related to that goal
     * @return the indexes as a list of array
     */
    static List<int[]> calculateNextBestIndexesToCheck(COREImpactNode goal, 
            EvaluationInformation evaluationInfo) {
        List<int[]> nextBestIndexesToAsk = new ArrayList<>();
        if (evaluationInfo.getLastCalculated() != -1) {
            EvaluationValueSet previousBestValueSet = new EvaluationValueSet();
            previousBestValueSet = evaluationInfo.getAchievableValueTable()
                    .get(evaluationInfo.getLastCalculated());
            int[] previousBestIndex = previousBestValueSet.getIndex();
            nextBestIndexesToAsk = ImpactModelEvaluationUtil.calculateNextIndexes(previousBestIndex);
        } else {
            int[] nextBestIndex = new int[goal.getIncoming().size()];
            for (int i = 0; i < nextBestIndex.length; i++) {
                nextBestIndex[i] = 0;
            }
            nextBestIndexesToAsk.add(nextBestIndex);
        }

        return nextBestIndexesToAsk;
    }
    
    /**
     * Given an index, calculates the next ones to check. i.e. for 0,0 returns 1,0 and 0,1
     * @param previousBestIndex is the index to be popped.
     * @return are the indexes to be pushed.
     */
    private static List<int[]> calculateNextIndexes(int[] previousBestIndex) {
        List<int[]> nextBestIndexesToAsk = new ArrayList<>(previousBestIndex.length);
        int[] tempPrev = previousBestIndex.clone();
        for (int i = 0; i < previousBestIndex.length; i++) {
            int[] anIndex = tempPrev.clone();
            anIndex[i] = previousBestIndex[i] + 1;
            nextBestIndexesToAsk.add(anIndex);
        }
        return nextBestIndexesToAsk;
    }
    
    /**
     * Given the parent feature, this function returns the type of relationship it has with its children.
     * @param parent is the parent feature to be checked.
     * @return null of the relationship is mandatory or optional.
     */
    static COREFeatureRelationshipType getRelationshipWithChildren(COREFeature parent) {
        COREFeatureRelationshipType retVal = null;
        if (parent.getChildren().size() != 0) {
            COREFeature child = parent.getChildren().get(0);
            if (child.getParentRelationship() == COREFeatureRelationshipType.XOR) {
                retVal = COREFeatureRelationshipType.XOR;                
            } else if (child.getParentRelationship() == COREFeatureRelationshipType.OR) {
                retVal = COREFeatureRelationshipType.OR;
            } else {
                retVal = null;
            }
        }
        return retVal;
    }
    
    /**
     * Recursively collects the features that are mandatory in the feature model.
     * @param feature starts with root feature.
     * @return the list of mandatory features.
     */
    static Set<COREFeature> collectMandatoryChildren(COREFeature feature) {
        Set<COREFeature> mandatoryChildren = new HashSet<>();
        if (feature.getChildren().size() > 0) {
            for (COREFeature child : feature.getChildren()) {
                if (child.getParentRelationship() == COREFeatureRelationshipType.MANDATORY) {
                    mandatoryChildren.addAll(collectMandatoryChildren(child));
                }
            }
        }
        //root feature is mandatory.
        mandatoryChildren.add(feature);

        return mandatoryChildren;
    }
    
    /**
     * Calculates the contribution of a feature to a goal.
     * @param feature is the contributing feature.
     * @param goal is the contributed goal.
     * @return the contribution from feature to goal.
     */
    static float getContributionToGoal(COREFeature feature, COREImpactNode goal) {
        int contribution = 0;

        for (COREContribution incomingContribution : goal.getIncoming()) {
            if (incomingContribution.getSource() instanceof COREFeatureImpactNode) {
                if (((COREFeatureImpactNode) incomingContribution.getSource()).getRepresents().equals(feature)) {
                    contribution = incomingContribution.getRelativeWeight();
                    break;
                }
            }
        }
        
        return contribution;
    }
}
